global proc AEalVolumeNodeTemplate(string $n)
{
    editorTemplate -beginScrollLayout;

    editorTemplate -label "Cache filename" -ann "The path to the volume cache (Field3D or OpenVDB) to load" -addControl "volumePath";
    editorTemplate -label "Detail" -ann "How finely to sample the cache, relative to the voxel size. 1 uses a step_size the same as the voxel size, 2 uses a step_size half of the voxel size (so twice as much detail), 0.5 uses a step_size twice the voxel size (so half as much detail)" -addControl "stepSize";
    editorTemplate -addControl "previewStyle";

    editorTemplate -beginLayout "Scattering" -collapse 0;
        editorTemplate -addControl "scattering";
        editorTemplate -addControl "g";
        editorTemplate -addControl "absorption";
    editorTemplate -endLayout;

    editorTemplate -beginLayout "Density" -collapse 0;
        editorTemplate -label "Layer" -addControl "densityLayer";
        editorTemplate -label "Remap" -addControl "densityRemap";
        editorTemplate -label "Input min" -addControl "densityInputMin";
        editorTemplate -label "Input max" -addControl "densityInputMax";
        editorTemplate -label "Clip max" -addControl "densityClipMax";
        editorTemplate -label "Bias" -addControl "densityBias";
        editorTemplate -label "Gain" -addControl "densityGain";
        editorTemplate -label "Output min" -addControl "densityOutputMin";
        editorTemplate -label "Output max" -addControl "densityOutputMax";
    editorTemplate -endLayout;

    
    
    editorTemplate -beginLayout "Emission" -collapse 0;
        editorTemplate -addControl "emissionStrength";
        editorTemplate -label "Layer" -addControl "temperatureLayer";
        editorTemplate -label "Remap" -addControl "temperatureRemap";
        editorTemplate -label "Input min" -addControl "temperatureInputMin";
        editorTemplate -label "Input max" -addControl "temperatureInputMax";
        editorTemplate -label "Clip max" -addControl "temperatureClipMax";
        editorTemplate -label "Bias" -addControl "temperatureBias";
        editorTemplate -label "Gain" -addControl "temperatureGain";
        editorTemplate -label "Output min" -addControl "temperatureOutputMin";
        editorTemplate -label "Output max" -addControl "temperatureOutputMax";
        editorTemplate -addControl "physicalIntensity";
    editorTemplate -endLayout;

    editorTemplate -addExtraControls;

    editorTemplate -endScrollLayout;
}